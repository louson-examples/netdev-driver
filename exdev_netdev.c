#include <linux/netdevice.h>
#include <linux/etherdevice.h>

static const char *EXDEV_NAME = "exdev%d";
static struct net_device *exdev_netdev = NULL;
#define MSG_SIZE 255

/* Net device callbacks */
static int exdev_open(struct net_device *dev);
static int exdev_stop(struct net_device *dev);
/* static netdev_tx_t exdev_start_xmit(struct sk_buff *skb, struct net_device *dev); */

/* Net device structure */
static const struct net_device_ops exdev_ops = {
	.ndo_open = exdev_open,
	.ndo_stop = exdev_stop,
	/* .ndo_start_xmit	= exdev_start_xmit, */
};

/* Net device private structure */
struct exdev_netdev_private
{
	int value;
};

/**
 * Open network interface
 */
int exdev_open(struct net_device *dev)
{
        printk(KERN_INFO "Open device\n");
        return 0;
}

/**
 * Stop network interface
 */
int exdev_stop(struct net_device *dev)
{
        printk(KERN_INFO "Stop device\n");
        return 0;
}

/* netdev_tx_t exdev_start_xmit(struct sk_buff *skb, struct net_device *dev) { */

/* } */

/**
 * Callback for network interface set-up
 */
void exdev_netdev_setup(struct net_device *dev)
{
        printk(KERN_INFO "Exdev network interface setup\n");
        ether_setup(dev);
}

/**
 * Network interface initialization
 */
int exdev_netdev_init(void)
{
        struct exdev_netdev_private *exdev_netdev_private = NULL;
        int ret;

        exdev_netdev = alloc_netdev(sizeof(struct exdev_netdev_private),
                                    EXDEV_NAME, NET_NAME_PREDICTABLE,
                                    exdev_netdev_setup);
        if (exdev_netdev == NULL) {
                printk(KERN_ERR "Failed to setup interface\n");
                return -ENOMEM;
        }

        /* Set private data */
        exdev_netdev_private = netdev_priv(exdev_netdev);
        exdev_netdev_private->value = 4;

        /* Set callbacks */
        exdev_netdev->netdev_ops = &exdev_ops;

        /* Set properties */
        exdev_netdev->mtu = MSG_SIZE;
        exdev_netdev->hard_header_len = 0;

	/* We need neither head nor tail room */
	exdev_netdev->needed_headroom = 0;
	exdev_netdev->needed_tailroom = 0;

	/* Interface doesn’t support ARP / MULTICAST */
	exdev_netdev->flags |= IFF_NOARP;
	exdev_netdev->flags &= ~IFF_MULTICAST;

	/* set up MAC addr */
	random_ether_addr(exdev_netdev->dev_addr);

	/* /\* Register the device to NAPI *\/ */
	/* netif_napi_add(exdev_netdev, &private->napi, edmr_napi_poll, NAPI_POLL_WEIGHT); */

        /* Register device */
	ret = register_netdev(exdev_netdev);
        if (ret != 0) {
		netdev_err(exdev_netdev, "error %d registering device\n", ret);
		return ret;
	}

        return 0;
}

int exdev_netdev_exit(void)
{
        unregister_netdev(exdev_netdev);

	/* /\* Unregister the device from NAPI *\/ */
	/* netif_napi_del(&private->napi); */

	free_netdev(exdev_netdev);
	exdev_netdev = NULL;

	return 0;
}
