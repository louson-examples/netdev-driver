#ifndef _EXDEV_NETDEV_H_
#define _EXDEV_NETDEV_H_

struct exdev_private {

};

int exdev_netdev_init(void);
int exdev_netdev_exit(void);

#endif  // _EXDEV_NETDEV_H_
