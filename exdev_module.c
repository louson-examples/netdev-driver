#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>

#include "exdev_netdev.h"

static int __init exdev_init(void) {
        printk(KERN_INFO "exdev: driver example\n");

        exdev_netdev_init();

        return 0;
}

static void __exit exdev_exit(void) {
        printk(KERN_INFO "exdev: exit\n");

        exdev_netdev_exit();

        return;
}

module_init(exdev_init);
module_exit(exdev_exit);

static struct 

MODULE_LICENSE("GPL");
MODULE_AUTHOR("louson <louis.rannou@gmail.com>");
MODULE_DESCRIPTION("Network driver example");
